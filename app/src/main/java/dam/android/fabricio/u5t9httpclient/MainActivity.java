package dam.android.fabricio.u5t9httpclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener{

    private final static  String URL_GEONAMES ="http://api.geonames.org/wikipediaSearchJSON";
    private final static String URL_WEATHER= "http://api.openweathermap.org/data/2.5/weather";
    private final static String API_KEY = "4adf2b709ad93bb80012a6f23a931c35";
    private final static  String USERNAME = "fabricio";
    private final static int ROWS = 10;

    private EditText etPlaceName;
    private Button btSearch;
    private ListView lvSearchResult;
    private GeonamesPlace geoPlace;
    private ArrayList<GeonamesPlace> listSearchResult;
    private ExecutorService executor;
    private String allResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUi();
    }
//codigo para acceder a los elementos de la UI
    private void setUi() {
        etPlaceName= findViewById(R.id.etPlaceName);
        btSearch = findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);

        listSearchResult = new ArrayList<>();

        lvSearchResult = findViewById(R.id.lvSearchResult);

        lvSearchResult.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,listSearchResult));

        lvSearchResult.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(isNetworkAvailable()){
            String place = etPlaceName.getText().toString();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(lvSearchResult.getWindowToken(), 0);
            if(!place.isEmpty()){
                URL url;
                try{
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.geonames.org")
                            .appendPath("wikipediaSearchJSON")
                            .appendQueryParameter("q", place)
                            .appendQueryParameter("maxRows", String.valueOf(ROWS))
                            .appendQueryParameter("lang","es")
                            .appendQueryParameter("username",USERNAME);
                    url= new URL(builder.build().toString());
                    startBackgroundTask(url);
                }catch (MalformedURLException e){
                    Log.i("URL", e.getMessage());
                }
            }else{
                Toast.makeText(this, "Wrtite a place to search!!", Toast.LENGTH_LONG).show();

            }
        }else
            Toast.makeText(this, "Sorry, network is nor available", Toast.LENGTH_LONG).show();
    }

    private void startBackgroundTask(final URL url) {
        final int CONNECTION_TIMEOUT = 10000;
        final int READ_TIMEOUT = 7000;

        executor = Executors.newSingleThreadExecutor();
        final Handler handler = new Handler(Looper.getMainLooper());
        if(url.getAuthority().contains("api.geonames.org")){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                final ArrayList<GeonamesPlace> searchResult = new ArrayList<>();
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();

                    urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                    urlConnection.setReadTimeout(READ_TIMEOUT);
                    urlConnection.connect();

                    getData(urlConnection, searchResult);

                } catch (IOException e) {
                    Log.i("IOException", e.getMessage());
                } catch (JSONException e) {
                    Log.i("JSONException", e.getMessage());
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (searchResult.size() > 0) {
                            ArrayAdapter<GeonamesPlace> adapter = (ArrayAdapter<GeonamesPlace>) lvSearchResult.getAdapter();
                            adapter.clear();
                            adapter.addAll(searchResult);
                            adapter.notifyDataSetChanged();
                        } else
                            Toast.makeText(getApplicationContext(), "Not possible to contact " + URL_GEONAMES, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }else{
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    HttpURLConnection urlConnection = null;
                    final ArrayList<String> searchResult = new ArrayList<>();
                    try {
                        urlConnection = (HttpURLConnection) url.openConnection();

                        urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                        urlConnection.setReadTimeout(READ_TIMEOUT);
                        urlConnection.connect();

                        getDataWeather(urlConnection, searchResult);

                    } catch (IOException e) {
                        Log.i("IOException", e.getMessage());
                    } catch (JSONException e) {
                        Log.i("JSONException", e.getMessage());
                    } finally {
                        if (urlConnection != null) urlConnection.disconnect();
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (searchResult.size() > 0) {
                                Toast.makeText(getApplicationContext(), allResult, Toast.LENGTH_LONG).show();
                            } else
                                Toast.makeText(getApplicationContext(), "Not possible to contact " + URL_GEONAMES, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });

        }

    }


    private void getData(HttpURLConnection urlConnection, ArrayList<GeonamesPlace> searchResult)
        throws IOException, JSONException{

            if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                String resultStram = readStream(urlConnection.getInputStream());

                JSONObject json = new JSONObject(resultStram);
                JSONArray jArray = json.getJSONArray("geonames");
                if(jArray.length()>0){
                    for (int i=0; i<jArray.length();i++){
                        JSONObject item = jArray.getJSONObject(i);
                        geoPlace = new GeonamesPlace(item.getString("summary"),item.getDouble("lat"),item.getDouble("lng"));
                        searchResult.add(geoPlace);
                    }
                }else
                    geoPlace = new GeonamesPlace("No information found ar geonames",0,0);
                    searchResult.add(geoPlace);
            }else{
                Log.i("URL", "ErrorCode: "+urlConnection.getResponseCode());
            }
        }

    private void getDataWeather(HttpURLConnection urlConnection, ArrayList<String> searchResult)
            throws IOException, JSONException{

        if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
            String resultStram = readStream(urlConnection.getInputStream());

            JSONObject json = new JSONObject(resultStram);
            JSONArray jArray = json.getJSONArray("weather");


            if(jArray.length()>0){


                for (int i=0; i<jArray.length();i++){
                    JSONObject item = jArray.getJSONObject(i);
                    allResult=item.getString("description");

                }
                allResult+= "\nTEMP: "+json.getJSONObject("main").get("temp");
                allResult+= "\nHUMIDITY: "+json.getJSONObject("main").get("humidity")+"%";
                searchResult.add(allResult);

            }else{
                Toast.makeText(getApplicationContext(), "Error ", Toast.LENGTH_LONG).show();
            }

        }else{
            Log.i("URL", "ErrorCode: "+urlConnection.getResponseCode());
        }
    }



    private String readStream(InputStream in) throws IOException{
        StringBuilder sb = new StringBuilder();


        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));

        String nextLine= "";
        while((nextLine = reader.readLine()) !=null ){
            sb.append(nextLine);
        }
        return sb.toString();
    }

    @SuppressWarnings("deprecation")
    private Boolean isNetworkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            Network nw = connectivityManager.getActiveNetwork();
            if(nw == null) return  false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));

        }else {
            NetworkInfo nwInfo =connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(executor !=null){
            executor.shutdown();
            Log.i("EXECUTOR", "ALL TASKS CAMCELLED!!!");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(isNetworkAvailable()){
        double latitud;
        double longitud;
        GeonamesPlace geonameplace =  listSearchResult.get(position);

        latitud=geonameplace.getLat();
        longitud = geonameplace.getLng();
           URL url;
                try{
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme("http")
                            .authority("api.openweathermap.org")
                            .appendPath("data")
                            .appendPath("2.5")
                            .appendPath("weather")
                            .appendQueryParameter("lat", String.valueOf(latitud))
                            .appendQueryParameter("lon",String.valueOf(longitud))
                            .appendQueryParameter("units","metric")
                            .appendQueryParameter("appid",API_KEY);
                    url= new URL(builder.build().toString());
                    startBackgroundTask(url);

                }catch (MalformedURLException e){
                    Log.i("URL", e.getMessage());
                }

        }
    }
}
package dam.android.fabricio.u5t9httpclient;

public class GeonamesPlace {
    private String summary;
    private double lat;
    private double lng;

    public GeonamesPlace(String summary, double lat, double lng) {
        this.summary = summary;
        this.lat = lat;
        this.lng = lng;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return
                 summary + '\n' +
                " Latitud: " + lat +
                ", Longitud: " + lng;
    }
}
